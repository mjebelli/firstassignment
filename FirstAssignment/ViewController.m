//
//  ViewController.m
//  FirstAssignment
//
//  Created by moha on 10/21/1396 AP.
//  Copyright © 1396 moha. All rights reserved.
//
#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIScrollView* scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    //scrollView.backgroundColor = [UIColor blueColor];
    
    [self.view addSubview:scrollView];
    
    
    
    
    [scrollView setContentSize:CGSizeMake(self.view.frame.size.width * 3.5, self.view.frame.size.height * 3.0)];
    
    UIView* redView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 500, 720)];
    redView.backgroundColor = [UIColor redColor];
    
    [scrollView addSubview:redView];
    
    
    
    UIView* yellowView = [[UIView alloc]initWithFrame:CGRectMake(500, 0, 500, 720)];
    yellowView.backgroundColor = [UIColor yellowColor];
    
    [scrollView addSubview:yellowView];
    
    
    UIView* blueView = [[UIView alloc]initWithFrame:CGRectMake(1000, 0, 500, 720)];
    blueView.backgroundColor = [UIColor blueColor];
    
    [scrollView addSubview:blueView];
    
    
    
    
    UIView* blackView = [[UIView alloc]initWithFrame:CGRectMake(0, 720, 500, 720)];
    blackView.backgroundColor = [UIColor blackColor];
    
    [scrollView addSubview:blackView];
    
    
    
    UIView* orangeView = [[UIView alloc]initWithFrame:CGRectMake(500, 720, 500, 720)];
    orangeView.backgroundColor = [UIColor orangeColor];
    
    [scrollView addSubview:orangeView];
    
    
    
    UIView* purpleView = [[UIView alloc]initWithFrame:CGRectMake(1000, 720, 500, 720)];
    purpleView.backgroundColor = [UIColor purpleColor];
    
    [scrollView addSubview:purpleView];
    
    
    
    
    UIView* greenView = [[UIView alloc]initWithFrame:CGRectMake(0, 1440, 500, 720)];
    greenView.backgroundColor = [UIColor greenColor];
    
    [scrollView addSubview:greenView];
    
    
    UIView* grayView = [[UIView alloc]initWithFrame:CGRectMake(500, 1440, 500, 720)];
    grayView.backgroundColor = [UIColor grayColor];
    
    [scrollView addSubview:grayView];
    
    
    UIView* brownView = [[UIView alloc]initWithFrame:CGRectMake(1000, 1440, 500, 720)];
    brownView.backgroundColor = [UIColor brownColor];
    
    [scrollView addSubview:brownView];
    
    
    
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

