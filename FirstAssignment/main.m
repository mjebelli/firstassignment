//
//  main.m
//  FirstAssignment
//
//  Created by moha on 10/21/1396 AP.
//  Copyright © 1396 moha. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
