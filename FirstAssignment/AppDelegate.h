//
//  AppDelegate.h
//  FirstAssignment
//
//  Created by moha on 10/21/1396 AP.
//  Copyright © 1396 moha. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

